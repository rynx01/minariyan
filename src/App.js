import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
    constructor() {
        super();

        this.state = {
            tweets: [],
        };
    }

    componentDidMount() {
        axios
            .get('http://localhost:3010/')
            .then(res => {
                console.log(res.data)
                return res.data
            })
            .then(json => {
                this.setState({ tweets: json });
            })
            .catch(error => {
                console.log(error);
            });

    }

    render() {

        const { tweets } = this.state;
        const loadPage = <div>
                            <div>
                                {tweets.map((tweet, i) => (
                                    <span key={i}>
                                        {tweet.text}<br/>
                                    </span>
                                ))}
                            </div>
                        </div>;

                return (
                    <div className="App">
                        <div className="minari-nav-bar">
                            <div className="minari-nav-container">
                                <li><i className="glyphicon glyphicon-home margin-icon" />Home</li>
                                <li><i className="glyphicon glyphicon-list-alt margin-icon" />Notifications</li>
                                <li><i className="glyphicon glyphicon-envelope margin-icon" />Messages</li>
                            </div>
                        </div>
                        <div className="minari-hashtag-container">
                            <h1 className="minari-hashtag">#mina</h1>
                            <div className="minari-nav-container second-nav">
                                <li className="margin-second-nav">Latest</li>
                                <li className="margin-second-nav">People</li>
                                <li className="margin-second-nav">Photos</li>
                                <li className="margin-second-nav">Videos</li>
                            </div>
                            <hr className="minari-hr" />
                        </div>
                        <div className="minari-content-container">
                            {loadPage}
                        </div>
                    </div>
                );
            }
    }

export default App;